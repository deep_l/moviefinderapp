package com.example.test.moviefinder;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.SimpleResource;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.test.moviefinder.Asynctasks.ApiCall;
import com.example.test.moviefinder.Models.ResultsModel;
import com.example.test.moviefinder.interfaces.AddToList;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements AddToList{


    @BindView(R.id.results)
    RecyclerView results;

    @BindView(R.id.titleName)
    EditText titleName;
    ArrayList<ResultsModel> resultsData;
    SearchResults searchAdapter;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        resultsData=new ArrayList<>();
        results.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        results.setItemAnimator(new DefaultItemAnimator());
        searchAdapter=new SearchResults(resultsData);
        results.setAdapter(searchAdapter);          //set customised adapter
        type=getIntent().getStringExtra("type");    //used to validate the search type
    }

    //api call to retrieve the list of requested data.
    public void callApi(View v){
        resultsData.clear();
        searchAdapter.notifyDataSetChanged();
        String inputData=titleName.getText().toString();
        titleName.setText("");
        int commas = 0;
        if(!inputData.isEmpty()){
            if(inputData.contains(",")){
                inputData+=",";
                for(int i = 0; i < inputData.length(); i++) {
                    if(inputData.charAt(i) == ','){
                        String data=inputData.substring(commas,i);
                        commas=i+1;
                        new ApiCall(this).execute(data.trim().replace(" ","%20"),"&type="+type);
                    }
                }
            }else
                new ApiCall(this).execute(inputData.trim().replace(" ","%20"),"&type="+type);
        }else{
            Toast.makeText(this, "Please enter a valid "+type+" name", Toast.LENGTH_SHORT).show();
        }
    }

    public class SearchResults extends RecyclerView.Adapter<MovieViewHolder>{
        ArrayList<ResultsModel> listData;
        public SearchResults(ArrayList<ResultsModel> resultsData){
            this.listData=resultsData;
        }

        @Override
        public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.search_model,parent,false);
            return new MovieViewHolder(view);
        }

        @Override
        public int getItemCount() {
            return listData.size();
        }




        @Override
        public void onBindViewHolder(final MovieViewHolder holder, int position) {
                final ResultsModel resultsModel=listData.get(position);
                holder.title.setText(resultsModel.title);
                holder.rating.setText(resultsModel.rating);
                Glide.with(getApplicationContext()).load(resultsModel.posterPath).into(holder.posterImage);
                holder.progressBar.setVisibility(View.VISIBLE);
                Glide.with(getApplicationContext())
                    .load(resultsModel.posterPath)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.posterImage);


                holder.mainLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent displayMovieDetails=new Intent(getApplicationContext(),DisplayMovieDetails.class);
                        displayMovieDetails.putExtra("movieTitle",resultsModel.title);
                        displayMovieDetails.putExtra("movieRatings",resultsModel.rating);
                        displayMovieDetails.putExtra("movieReleased",resultsModel.releaseDate);
                        displayMovieDetails.putExtra("movieGenre",resultsModel.genre);
                        displayMovieDetails.putExtra("moviePlot",resultsModel.plot);

                        GlideBitmapDrawable drawable = (GlideBitmapDrawable) holder.posterImage.getDrawable();
                        Bitmap bitmap = drawable.getBitmap();
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        displayMovieDetails.putExtra("moviePoster",byteArray);

                        ActivityOptionsCompat options = ActivityOptionsCompat.
                                makeSceneTransitionAnimation(MainActivity.this, (View)holder.posterImage, "moviePoster");
                        startActivity(displayMovieDetails,options.toBundle());
                    }
                });
        }
    }


    public class MovieViewHolder extends RecyclerView.ViewHolder{

        TextView title,rating;
        ImageView posterImage;
        RelativeLayout mainLayout;
        ProgressBar progressBar;
        public MovieViewHolder(View itemView) {
            super(itemView);
            mainLayout=(RelativeLayout)itemView.findViewById(R.id.mainLayout);
            title=(TextView)itemView.findViewById(R.id.modelTitle);
            rating=(TextView)itemView.findViewById(R.id.modelrating);
            posterImage=(ImageView) itemView.findViewById(R.id.posterImage);
            progressBar=(ProgressBar)itemView.findViewById(R.id.progressBar);
        }
    }

    @Override
    public void addToList(ResultsModel resultsModel) {
        resultsData.add(resultsModel);
        searchAdapter.notifyDataSetChanged();
    }

    @Override
    public void invalidMovieName(String movieName) {
        Toast.makeText(this, movieName+" is not a valid movie name. Please try again", Toast.LENGTH_SHORT).show();
    }
}
