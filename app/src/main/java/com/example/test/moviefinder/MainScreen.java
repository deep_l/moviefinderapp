package com.example.test.moviefinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jay.l on 29/04/17.
 */

public class MainScreen extends AppCompatActivity {

    @BindView(R.id.movies)
    TextView selectMovies;

    @BindView(R.id.series)
    TextView selectSeries;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.movies)
    public void openSeries(View v){
        Intent goToSearch=new Intent(this,MainActivity.class);
        goToSearch.putExtra("type","movie");
        startActivity(goToSearch);
    }

    @OnClick(R.id.series)
    public void selectSeries(View v){
        Intent goToSearch=new Intent(this,MainActivity.class);
        goToSearch.putExtra("type","series");
        startActivity(goToSearch);
    }
}
