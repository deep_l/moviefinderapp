package com.example.test.moviefinder.Models;

/**
 * Created by jay.l on 23/04/17.
 */

public class ResultsModel {
   public String title,genre,posterPath,releaseDate,plot,rating;

    public ResultsModel(String title,String posterPath,String genre,String releaseDate,String plot,String rating){
        this.title=title;
        this.posterPath=posterPath;
        this.genre=genre;
        this.releaseDate=releaseDate;
        this.plot=plot;
        this.rating=rating;
    }
}
