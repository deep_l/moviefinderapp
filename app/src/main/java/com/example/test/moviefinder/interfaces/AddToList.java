package com.example.test.moviefinder.interfaces;

import com.example.test.moviefinder.Models.ResultsModel;

import java.util.ArrayList;

/**
 * Created by jay.l on 23/04/17.
 */

public interface AddToList{
     void addToList(ResultsModel resultsModel);
    void invalidMovieName(String movieName);
}
