package com.example.test.moviefinder;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jay.l on 28/04/17.
 */

public class DisplayMovieDetails extends AppCompatActivity {

    @BindView(R.id.moviePoster)
    ImageView moviePoster;

    @BindView(R.id.movieTitle)
    TextView movieTitle;

    @BindView(R.id.movieRatings)
    TextView movieRatings;

    @BindView(R.id.movieReleased)
    TextView movieReleased;

    @BindView(R.id.movieGenre)
    TextView movieGenre;

    @BindView(R.id.moviePlot)
    TextView moviePlot;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_movie_details);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        byte[] byteArray = extras.getByteArray("moviePoster");

        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        moviePoster.setImageBitmap(bmp);
        movieTitle.setText(getIntent().getStringExtra("movieTitle"));
        movieRatings.setText(getIntent().getStringExtra("movieRatings"));
        movieReleased.setText(getIntent().getStringExtra("movieReleased"));
        movieGenre.setText(getIntent().getStringExtra("movieGenre"));
        moviePlot.setText(getIntent().getStringExtra("moviePlot"));
    }
}
