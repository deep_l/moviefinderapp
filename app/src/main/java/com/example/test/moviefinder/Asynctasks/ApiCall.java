package com.example.test.moviefinder.Asynctasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.load.engine.cache.MemoryCache;
import com.example.test.moviefinder.Constants;
import com.example.test.moviefinder.Models.ResultsModel;
import com.example.test.moviefinder.interfaces.AddToList;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by jay.l on 23/04/17.
 */

public class ApiCall extends AsyncTask<String,Void,String> {

    AddToList addToList;
    String movieName,indicator;

    public ApiCall(Activity activity) {
        addToList = (AddToList) activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try{
            if(s!=null){
                JSONObject jsonResults=new JSONObject(s);
                if(jsonResults.getString("Response").equals("True")){   //valid input by the user.
                    String ratings;
                    if(indicator.contains("movie")){
                        JSONArray jsonRatings=jsonResults.getJSONArray("Ratings");
                        JSONObject raingsImdb=jsonRatings.getJSONObject(0);
                        ratings=raingsImdb.getString("Value");
                    }else{
                        ratings=jsonResults.getString("imdbRating");
                    }
                    String title=jsonResults.getString("Title");
                    String releasedData=jsonResults.getString("Released");
                    String imagePath=jsonResults.getString("Poster");
                    String genre=jsonResults.getString("Genre");
                    String plot=jsonResults.getString("Plot");
                    addToList.addToList(new ResultsModel(title,imagePath,genre,releasedData,plot,ratings));
                }else
                    addToList.invalidMovieName(movieName);  //invalid movie or series name entered
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        try{
            movieName=params[0];
            indicator=params[1];
            URL url = new URL(Constants.api_url+"?t="+movieName+indicator);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            int responseCode = urlConnection.getResponseCode();

            if(responseCode==HttpURLConnection.HTTP_OK){
                BufferedReader reader=new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuffer response = new StringBuffer();
                String line="";
                while ((line=reader.readLine())!=null){
                    response.append(line);
                }
                return response.toString();
            }
            return null;

        }catch (Exception e){
                e.printStackTrace();
        }
        return null;
    }
}
